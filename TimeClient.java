import java.util.Scanner;
import java.net.Socket;
import java.io.IOException;

public class TimeClient{
    public static void main(String args[]){

	final String HOST;
	final int PORT;

	/* If the host and port number were given as commandline
	   arguments use them, otherwise use this computer */
	if (args.length == 2) {
	    HOST = args[0];
	    PORT = Integer.parseInt(args[1]);
	} else {	    
	    HOST = "localhost";
	    PORT = 13;
	}

	try{
	
	    Socket socket = new Socket(HOST, PORT);
	    Scanner netInput = new Scanner(socket.getInputStream());
	    System.out.println(netInput.nextLine());
	    socket.close();
	
	} catch(IOException ex){
	    System.out.println(ex.getMessage());
	}
    }
}
